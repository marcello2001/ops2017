public class Robot {
    public static void main (String [] args) {
        /*for(String s: args) {
            System.out.println(s);
        }*/

        String comandi = "";
        int orientamento = Integer.parseInt(args[0]);

        String [] coordinate = args[1].split(",");
        int x = Integer.parseInt(coordinate[0]);
        int y = Integer.parseInt(coordinate[1]);

        for (int i = 2; i < args.length - 1; i++) {
            //System.out.println(args[i]);
            coordinate = args[i].split(",");
            int x1 = Integer.parseInt(coordinate[0]);
            int y1 = Integer.parseInt(coordinate[1]);

            int n = x1 < x ? 3 : x1 > x ? 1: y1 < y? 2: y1 > y? 0: -1;
            comandi += orienta(orientamento, n);
            comandi += "f,";

            orientamento = n;
            x=x1;
            y=y1;

            //System.out.println(comandi);
            //System.out.println(orientamento);

        }
        comandi += orienta(orientamento, Integer.parseInt(args[args.length-1]));
        System.out.println(comandi);
    }
    static String orienta (int or, int n) {
        String ret = "";
        if (n == -1) return "ERRORE";
        if (or != n) {
            if ( (or + 1) % 4 == n) ret += "o,";
            else {
                do {
                    or = (or + 3) % 4;
                    ret += "a,";
                } while (or != n);
            }
        }
        return ret;
    }
}
