public class Grafi {
    static Collegamento [] nodi = null;

    public static void main(String[] args) {
        arco(1,2,5);
        arco(3,2,6);
        arco(1,3,7);
        arco(4,5,3);
        arco(5,6,4);
        arco(6,4,8);
        arco(5,2,8);
        arco(6,3,11);
        arco(1,4,5);

        int partenza = 1;
        int arrivo = 1;

        new Percorso (new Collegamento [] {trova(partenza)}, trova(arrivo).nodo).stampa();
    }
    static void arco (int nodo1, int nodo2, int distanza) {
        Nodo n1 = null, n2 = null;
        if(nodi != null) {
        for (Collegamento n: nodi)
            if (n.nodo.ugualea (nodo1))
                n1 = n.nodo;
            else if (n.nodo.ugualea (nodo2))
                n2 = n.nodo;
        }
        if (n1 == null) {
            if (n2 == null) {
                n1 = new Nodo (nodo1);
                n2 = new Nodo (nodo2, n1, distanza);
                nodi = Nodo.add (nodi, n2, 0);
            }
            else {
                n1 = new Nodo (nodo1, n2, distanza);
            }

            nodi = Nodo.add (nodi, n1, 0);
        } else if (n2 == null) {
            n2 = new Nodo (nodo2, n1, distanza);
            nodi = Nodo.add (nodi, n2, 0);
        } else {
            n1.collega(n2, distanza);
        }

    }
    static Collegamento trova (int n) {
        for (Collegamento c: nodi)
            if (c.portaa(n)) return c;

        return null;
    }

}

class Collegamento {
    public Nodo nodo;
    public int lunghezza;

	/**
	* Default Collegamento constructor
	*/
	public Collegamento(Nodo nodo, int lunghezza) {
		this.nodo = nodo;
		this.lunghezza = lunghezza;
	}
    boolean portaa (int n) {
        return nodo.ugualea(n);
    }
    boolean portaa (Nodo n) {
        return nodo.ugualea(n);
    }
}

class Nodo {
    public int nome;
    public Collegamento collegati [];

    public Nodo (int nome) {
        this.nome = nome;
        collegati = null;
    }
	/**
	* Default Nodo constructor
	*/
	public Nodo(int nome, Nodo n, int distanza) {
		this (nome);
        collega(n, distanza);
	}

    public void collega (Nodo n, int distanza) {
        aggiungi(n, distanza);
        n.aggiungi(this, distanza);
    }

    private void aggiungi (Nodo n, int distanza) {
        // Controlla che non sia gia' collegato a quel nodo (improbabile)
        if(collegati != null)
        for (Collegamento coll: collegati) if(coll.nodo.ugualea(n)) return;

        collegati = add (collegati, n, distanza);

    }

    static Collegamento[] add (Collegamento [] array, Nodo elemento, int lunghezza) {
        int numero = array != null ? array.length: 0;
        Collegamento [] a = new Collegamento [numero + 1];
        int indice = 0;
        if (array != null)
        for (Collegamento i: array)
            a[indice++] = i;
        a[indice] = new Collegamento(elemento, lunghezza);
        return a;
    }
    public boolean ugualea (Nodo n) {
        return n.nome == nome;
    }
    public boolean ugualea(int n) {
        return nome == n;
    }

    @Override
    public String toString () {
        return "n"+nome;
    }
}

class Percorso {
    String [] stringhe;
    int [] lunghezze;

    public Percorso (Collegamento [] itinerario, Nodo dest) {

        int indice = 0;
        Collegamento [] nuovo = new Collegamento[itinerario.length + 1];
        for (Collegamento c: itinerario) {
            nuovo[indice++] = c;
        }
        for (Collegamento c: itinerario[itinerario.length - 1].nodo.collegati) {
            //System.out.println(itinerario[itinerario.length - 1].nodo + "; Provo con "+ c.nodo);
            nuovo[itinerario.length] = c;

            if (c.portaa(dest)) {
                salva(nuovo);
                continue;
            }

            boolean nonpercorso = true;
            for (Collegamento coll: itinerario) {
                if (coll.portaa(c.nodo))
                    nonpercorso = false;
            }

            if (nonpercorso) {
                Percorso p = new Percorso (nuovo, dest);

                int n1 = stringhe != null? stringhe.length: 0;
                int n2 = p.stringhe != null? p.stringhe.length: 0;

                String [] sa = stringhe;
                int [] ia = lunghezze;
                stringhe = new String [n1 + n2];
                lunghezze = new int [n1 + n2];

                indice = 0;
                 for (int i = 0; i < n1; i++) {
                    stringhe[indice] = sa[i];
                    lunghezze[indice++] = ia[i];
                }
                for (int i = 0; i < n2; i++) {
                    stringhe[indice] = p.stringhe[i];
                    lunghezze[indice++] = p.lunghezze[i];
                }
            }
        }
    }
    private void salva (Collegamento [] itinerario) {
        String percorso = "[";
        int lunghezza = 0;
        for (Collegamento c: itinerario) {
            percorso += c.nodo + ",";
            lunghezza += c.lunghezza;
        }
        percorso += "]";
        String [] sa = stringhe;
        int[] ia = lunghezze;

        int n = stringhe != null ? stringhe.length: 0;
        stringhe = new String [n + 1];
        lunghezze = new int [n + 1];

        for (int i = 0; i < n; i++) {
            stringhe[i] = sa[i];
            lunghezze[i] = ia[i];
        }
        stringhe[stringhe.length - 1] = percorso;
        lunghezze[lunghezze.length - 1] = lunghezza;
    }
    public void stampa () {
        int i = 0;
        for (String s: stringhe) {
            System.out.println(lunghezze[i++] + "\t" + s);
        }
    }
}
