import java.util.ArrayList;
import java.util.ListIterator;

public class Scacchi {
    static int MAX_X = 5;
    static int MAX_Y = 5;

    static int [] partenza = new int [] {1,1};
    static int [] arrivo = new int [] {5,5};

    static ArrayList<int[]> proibite = new ArrayList<int[]>();
    static ArrayList<int[]> premi = new ArrayList<int[]>();


    public static void main(String[] args) {
        proibite.add(new int[]{1,2});
        proibite.add(new int[]{1,3});
        proibite.add(new int[]{1,4});
        proibite.add(new int[]{2,4});
        proibite.add(new int[]{3,4});
        proibite.add(new int[]{4,1});
        proibite.add(new int[]{4,2});
        proibite.add(new int[]{4,4});
        proibite.add(new int[]{4,5});
        proibite.add(new int[]{5,4});


        premi.add(new int[]{3,1,10});
        premi.add(new int[]{2,2,12});
        premi.add(new int[]{2,3,13});
        ArrayList<int[]> percorso= new ArrayList<int[]>();
        trovaPercorsi(partenza, arrivo, percorso, 0);

    }



    static void trovaPercorsi (int[]p, int[]a, ArrayList<int[]> perco, int premi) {
        premi += premio(p);
        ArrayList<int[]> percorso = new ArrayList<int[]>(perco);
        percorso.add(p);
        if(controlla(p,a)) {
            System.out.print(premi + "\t[");
            ListIterator<int[]> g = percorso.listIterator();
            while (g.hasNext()) System.out.print(stampa(g.next()));
            System.out.println("]");
            return;
        }
        int x=p[0], y=p[1];
        ArrayList<int[]> successive = new ArrayList<int[]>();
        successive.add(new int[]{ x+1, y+2 });    //nne
        //successive.add(new int[]{ x+2, y+1 });    //ene
        successive.add(new int[]{ x+2, y-1 });    //ese
        successive.add(new int[]{ x+1, y-2 });    //sse
        successive.add(new int[]{ x-1, y-2 });    //sso
        //successive.add(new int[]{ x-2, y-1 });    //oso
        successive.add(new int[]{ x-2, y+1 });    //ono
        //successive.add(new int[]{ x-1, y+2 });    //nno

        ListIterator<int[]> i = successive.listIterator();
        int[] pos;
        while(i.hasNext()) {
            pos = i.next();
            if(permessa(pos, percorso)) {
                //System.out.println("sposto a "+stampa(pos));
                trovaPercorsi(pos, a, percorso, premi);
            }
        }

    }




    static int premio (int[]a) {
        int i = verificaPos(a, premi);
        if(i==-1)return 0;
        return premi.get(i)[2];
    }
    static boolean permessa (int[]a, ArrayList<int[]> p) {
        boolean ret = verificaPos(a, proibite)!=-1? false: a[0]>MAX_X? false: a[1]>MAX_Y? false: a[0]<=0? false: a[1]<=0?false: true;
        ListIterator<int[]> i = p.listIterator();
        while(i.hasNext()) {
            if(controlla(a,i.next())) ret=false;
        }
        //return (verificaPos(a, proibite)==-1)&&(a[0]<=MAX_X)&&(a[1]<=MAX_Y);
        return ret;
    }
    static int verificaPos (int[]a, ArrayList<int[]> b) {
        int ret = -1;
        for (int i = 0; i < b.size(); i++) {
            if (controlla(a, b.get(i))) ret = i;
        }
        return ret;
    }
    static boolean controlla (int[]a, int[]b) {
        if (a[0] == b[0] && a[1] == b[1])
            return true;
        else
            return false;
    }
    static String stampa(int[]a) {
        return "["+a[0]+","+a[1]+"],";
    }
}
