 public class Deduzioni {

    static Regola [] regole;
    static String noti;
    static final String INESISTENTE = "INESISTENTE";

    public static void main(String[] args) {
        try {
            System.out.println("Ciao, adesso ti farò perdere tempo!");
            for (int i = 5; i > 0; i--) {
                System.out.println(i);
                Thread.sleep(1000);
            }
            System.out.println("AHAHAHAHAHA Sfigato dai mettiamoci al lavoro");

            regole = new Regola [] {
                new Regola (1, "ud", 'c'),
                new Regola (2, "qn", 'g'),
                new Regola (3, "pq", 'n'),
                new Regola (4, "cd", 'z'),
                new Regola (5, "u", 'd'),
                new Regola (6, "nu", 'm'),
            };
            noti = "u";
            System.out.println(trova('z', ""));


        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    static boolean esiste_una_regola = false;
    static boolean funziona = false;

    static String trova (char c, String usate) {
        String [] possibili = new String [regole.length];
        int indice = 0;

        boolean gia_noto = false;
        for (char noto: noti.toCharArray()) {
            System.out.println("trova: Cerco "+ c +": "+noto);
            if (c == noto) {
                gia_noto = true;
                System.out.println("trova: Trovato "+ c);
                possibili[indice++] = usate;
                break;
            }
            System.out.println("trova: Non dovrebbe stampare dopo averlo trovato");
        }
        if (gia_noto) return usate;
        else {
            esiste_una_regola = false;
            for (Regola regola: regole) {
                funziona = false;
                String s = controlla(regola, c, usate);
                System.out.println("Controllato " + regola);
                if (funziona) {
                    System.out.println("Trovato "+ s);
                    esiste_una_regola = true;
                    possibili [indice++] = s;
                }
            }
        }
        if (!esiste_una_regola && indice == 0) {
            return INESISTENTE;
        }
        int piu_breve = possibili[0].length();
        int indice_breve = 0;
        for (int j = 0; j < indice; j++) {
            System.out.println(j);
            if(possibili[j].length() < piu_breve) {
                piu_breve = possibili[j].length();
                indice_breve = j;
            }
        }
        return possibili[indice_breve];
    }


    static String controlla(Regola regola, char c, String usate) {
        if (regola.dedotto == c) {
            usate += " " + regola;
            System.out.println("Trovato regola "+ regola +" per trovare "+ c + "; siamo a " + usate);
            funziona = true;
            esiste_una_regola = true;

            boolean foglie = true;
            char [] requisiti = regola.requisiti.toCharArray();

            for (int i = 0; i < requisiti.length; i++) {
                /*
                boolean gia_noto = false;
                for (char noto: noti.toCharArray()) {
                    System.out.println("Cerco "+ requisiti[i] +": "+noto);
                    if (requisiti[i] == noto) {
                        gia_noto = true;
                        System.out.println("Trovato "+ requisiti[i]);
                        break;
                    }
                    System.out.println("Non dovrebbe stampare dopo averlo trovato");
                }
                if (!gia_noto) {
                    System.out.println ("Esegue solo se non è noto");
                    foglie = false;
                    */
                    System.out.println(noti);
                    String nuova = trova (requisiti[i], usate);
                    System.out.println("Cercando "+requisiti[i]+" ottengo " + nuova);
                    if (nuova.equals(INESISTENTE)) {
                        funziona = false;
                        System.out.println("OPS (<- lol) "+ c + " " + usate);
                        return INESISTENTE;
                    } else {
                        if (!usate.equals(nuova))
                            noti += requisiti[i];
                        usate = nuova;

                    }
                //}
            }
            esiste_una_regola = true;
            funziona = true;

        } return usate;
    }

}
