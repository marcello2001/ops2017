import java.util.ArrayList;
import java.util.ListIterator;

public class Deduzioni2 {
    static ArrayList<Regola> regole = new ArrayList<Regola>();
    static String noti = "";

    public static void main (String [] args) {
        regole.add(new Regola (1, "ud", 'c'));
        regole.add(new Regola (2, "qn", 'g'));
        regole.add(new Regola (3, "pq", 'n'));
        regole.add(new Regola (4, "cd", 'z'));
        regole.add(new Regola (5, "u", 'd'));
        regole.add(new Regola (6, "nu", 'm'));

        noti = "u";

        for (String s: risolvi('z')) {
            if (s == null) break;
            System.out.println(s);
        }

    }

    public static String[] risolvi (char c) {
        String [] s = new String[1000];
        s[0] = ".";
        if(noti.indexOf(c) >= 0) return s;
        ArrayList<Regola> r = trovaRegole (c);
        if (r == null) {
            System.out.println("Errore");
            s[0] = "ERRORE";
            return s;
        }
        ListIterator<Regola> i = r.listIterator();
        int j = 0;
        while(i.hasNext()) {
            System.out.println ("Checkpoint alpha");
            Regola regola = i.next();
            char [] requisiti = regola.requisiti.toCharArray();
            String [][] stringhe = new String [requisiti.length][];
            int k = 0;
            for (char ch: requisiti) {
                stringhe [k++] = risolvi(ch);
            }

            System.out.println("Checkpoint Bravo, stampo b:");
            int a [] = new int [requisiti.length];
            for (int n: a)
                n = 0;
            int b [] = new int [requisiti.length];
            for (int n = 0; n < requisiti.length; n++) {
                int conto = 0;
                for (String str: stringhe[n]) {
                    if (str == null) break;
                    conto++;
                }
                b[n] = conto;
                System.out.println ("\t"+conto);
            }
            int contozx = 1;
            while (a[requisiti.length-1] < b[requisiti.length-1]) {
                System.out.println ("Checkpoint Charlie, prova " + contozx++);
                s[j] = "";
                boolean devo_aumentare = true;
                for (int n = 0; n < requisiti.length; n++) {
                    if (stringhe [n][a[n]] == null) {
                        devo_aumentare = false;
                        break;
                    }
                    if (!stringhe [n][a[n]].contains("ERRORE"))
                        s[j] += stringhe [n][a[n]];
                }
                if (devo_aumentare)
                    s[j++] += regola;
                //System.out.println (s[j++]);
                for (int x = requisiti.length - 1; x > 0; x--) {
                    if (a[x-1] == b[x-1] - 1) {
                        a[x-1] = 0;
                        a[x]++;
                    }

                }
                a[0]++;

            }
        }
        noti += c;
        return s;

    }

    public static ArrayList<Regola> trovaRegole (char c) {
        ArrayList <Regola> ret = new ArrayList <Regola>();
        boolean esiste = false;
        ListIterator <Regola> i = regole.listIterator();
        while (i.hasNext()) {

            Regola r = i.next();
            if (r.dedotto() == c) {
                ret.add(r);
                esiste = true;
            }
        }
        if (!esiste) return null;
        return ret;
    }
}
