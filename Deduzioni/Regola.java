public class Regola  {
    public final int nome;
    public final String requisiti;
    public final char dedotto;

    public Regola (int nome, String requisiti, char dedotto) {
        this.nome = nome;
        this.requisiti = new String (requisiti);
        this.dedotto = dedotto;
    }

    @Override
    public String toString () {
        return nome+",";
    }

    public int nome() {
        return nome;
    }
    public char dedotto() {
        return dedotto;
    }
}
