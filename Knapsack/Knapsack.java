import java.util.ArrayList;
import java.util.ListIterator;

public class Knapsack {

    static ArrayList<Minerale> minerali = new ArrayList<Minerale>();
    static ArrayList<Minerale> massima = new ArrayList<Minerale>();
    static int valore = 0;


    public static void main (String [] args) {
        try{
            minerali.add(new Minerale(1,39,58));
            minerali.add(new Minerale(2,42,64));
            minerali.add(new Minerale(3,40,65));
            minerali.add(new Minerale(4,38,59));
            minerali.add(new Minerale(5,37,61));
            minerali.add(new Minerale(6,42,62));

            ArrayList<Minerale> usati = new ArrayList<Minerale>();
            massimoValore(180, 3, usati);
            ListIterator<Minerale> i = massima.listIterator(massima.toArray().length);

            while(i.hasPrevious()) {
                System.out.print(i.previous());
            }
            System.out.println("\n" +valore);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void massimoValore (int peso, int numero, ArrayList<Minerale> usati) throws Exception {
        //System.out.println("Inizio! Devo sistemare "+ numero+" minerali!");
        if (numero <= 0) return;
        ListIterator<Minerale> i = minerali.listIterator(0);
        while(i.hasNext()) {
            //System.out.println("Provo");
            ArrayList<Minerale> a = new ArrayList<Minerale>(usati);
            if(usati.contains(i.next())) return;
            i.previous();
            a.add(i.next());

            if(numero == 1) {
                //System.out.print("Sistemato l'ultimo minerale, valore ");
                ListIterator<Minerale> j = a.listIterator(0);
                int peso1 = 0;
                int valore1 = 0;
                while(j.hasNext()) {
                    peso1 += j.next().peso;
                    valore1 += j.previous().valore;
                    j.next();
                }
                //System.out.println(valore1+" peso "+peso1);
                if(peso1 <= peso) {
                    if (valore1 > valore) {
                        //System.out.println("Valore maggiore!");
                        valore = valore1;
                        massima = new ArrayList<Minerale>(a);
                    }
                }
            }
            massimoValore (peso, numero-1, a);
        }
    }
}

class Minerale {
    public int nome;
    public int peso;
    public int valore;

    public Minerale (int nome, int valore, int peso) {
        this.nome = nome;
        this.peso = peso;
        this.valore = valore;
    }
    @Override
    public String toString () {
        return "m"+nome+",";
    }
}
